#
# testinfra tests for ctfd.ctf
#

from utils import *
import common


def test_redis_package(host):
    is_installed(host, 'redis-server')


def test_redis_service(host):
    is_running_and_enabled(host, 'redis-server')


def test_redis_listening(host):
    is_listening(host, 'tcp://127.0.0.1:6379')


def test_nginx_package(host):
    is_installed(host, 'nginx-light')


def test_nginx_service(host):
    is_running_and_enabled(host, 'nginx')


def test_nginx_listening(host):
    is_listening(host, 'tcp://0.0.0.0:80')
    is_listening(host, 'tcp://0.0.0.0:443')


def test_gunicorn_running(host):
    p = host.process.filter(user='ctfd')
    assert(p)
    assert(len(p) > 2)


def test_gunicorn_listening(host):
    is_listening(host, 'tcp://127.0.0.1:8000')


def test_rules(host):
    common.std_rules(host)
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 80 -m comment --comment "incoming http" -j ACCEPT')
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 443 -m comment --comment "incoming https" -j ACCEPT')
    has_rule(host, '-A OUTPUT -d 10.0.0.0/8 -o eth0 -m comment --comment "drop to internal" -j DROP')
    has_rule(host, '-A OUTPUT -d 169.254.0.0/16 -o eth0 -m comment --comment "drop to internal .inf" -j DROP')
    has_rule(host, '-A OUTPUT -o eth0 -p tcp -m tcp --dport 443 -m owner --uid-owner 0 -m comment --comment "ctf/ctfd refresh" -j ACCEPT')


def test_nginx_reachable(host):
    can_connect(host, 'eth0', 80)
    can_connect(host, 'eth0', 443)


def test_ctfd_not_reachable(host):
    cannot_connect(host, 'eth0', 8000)


def test_ssh_not_reachable(host):
    common.ssh_not_reachable(host, 'eth0')
