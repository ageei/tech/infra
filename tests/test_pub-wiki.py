#
# testinfra tests for wiki.pub
#

from utils import *
import common
import os
import ssl


def test_nginx_package(host):
    is_installed(host, 'nginx-light')


def test_nginx_running_and_enabled(host):
    is_running_and_enabled(host, 'nginx')


def test_nginx_listening_80(host):
    is_listening(host, 'tcp://0.0.0.0:80')


def test_nginx_listening_443(host):
    is_listening(host, 'tcp://0.0.0.0:443')


def test_gitit_installed(host):
    is_installed(host, 'gitit')


def test_gitit_running_and_enabled(host):
    is_running_and_enabled(host, 'gitit')


def test_gitit_listening(host):
    is_listening(host, 'tcp://127.0.0.1:5001')


def test_rules(host):
    common.std_rules(host)
    has_rule(host, '-A OUTPUT -d 169.254.0.0/16 -o eth0 -j DROP')
    has_rule(host, '-A OUTPUT -d 10.0.0.0/8 -o eth0 -j DROP')
    has_rule(host, '-A OUTPUT -o eth0 -p tcp -m tcp --dport 443 -m owner --uid-owner 0 -m comment --comment "outgoing https" -j ACCEPT')


try:
    from http.client import HTTPSConnection
except ImportError:
    from httplib import HTTPSConnection

def test_wiki_export(host):
    addr = host.interface('eth0').addresses[0]
    sctx = ssl.SSLContext()
    conn = HTTPSConnection(addr, 443, context=sctx)
    conn.request('POST', '/Front%20Page', 'export=Export&format=Man+page')
    status = conn.getresponse().status
    conn.close()
    assert(200 == status)


def test_ssh_not_reachable(host):
    common.ssh_not_reachable(host, 'eth0')


def test_gitit_not_reachable(host):
    cannot_connect(host, 'eth0', 5001)
