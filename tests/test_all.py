#
# testinfra tests common to ALL hosts.
#

from utils import *


def test_common_rules(host):
    ruleset = [
        '-P INPUT DROP',
        '-P FORWARD DROP',
        '-P OUTPUT DROP',
        '-A INPUT -i lo -j ACCEPT',
        '-A OUTPUT -o lo -j ACCEPT',
        '-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT',
        '-A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT',
    ]
    [has_rule(host, r) for r in ruleset]


def test_salt_minion_package(host):
    is_installed(host, 'salt-minion')


def test_salt_minion_service(host):
    is_running_and_enabled(host, 'salt-minion')


def test_syslog_udp(host):
    is_listening(host, 'udp://0.0.0.0:514')


def test_internet_dns(host):
    a = host.addr('uqam.ca')
    assert(a.is_resolvable)


def test_telegraf_to_tig(host):
    f = host.file('/etc/telegraf/telegraf.d/common.conf')
    assert(f.exists)
    assert(f.is_file)
    assert(f.contains('http://tig.inf:8086'))
