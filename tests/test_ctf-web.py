#
# testinfra tests for web.ctf
#

from utils import *
import common


def test_docker_installed(host):
    is_installed(host, 'containerd.io')
    is_installed(host, 'docker-ce')
    is_installed(host, 'docker-ce-cli')


def test_docker_service(host):
    is_running_and_enabled(host, 'docker')


def test_docker_socket(host):
    is_listening(host, 'unix:///var/run/docker.sock')


def test_haproxy_package(host):
    is_installed(host, 'haproxy')


def test_haproxy_service(host):
    is_running_and_enabled(host, 'haproxy')


def test_haproxy_listening(host):
    is_listening(host, 'tcp://0.0.0.0:80')


def test_rules(host):
    common.std_rules(host)
    # security: no internal traffic allowed, internet:443 is allowed
    has_rule(host, '-A OUTPUT -d 10.0.0.0/8 -o eth0 -m comment --comment "drop to internal" -j DROP')
    has_rule(host, '-A OUTPUT -d 169.254.0.0/16 -o eth0 -m comment --comment "drop to internal .inf" -j DROP')
    has_rule(host, '-A OUTPUT -o eth0 -p tcp -m tcp --dport 443 -m owner --uid-owner 0 -m comment --comment "ctf/web refresh" -j ACCEPT')
    # functionality: port 80 is reachable
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 80 -m comment --comment "incoming http" -j ACCEPT')


def test_haproxy_reachable(host):
    can_connect(host, 'eth0', 80)


# TODO sweep port range
#def test_containers_not_reachable(host):


def test_ssh_not_reachable(host):
    common.ssh_not_reachable(host, 'eth0')
