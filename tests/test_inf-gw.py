#
# testinfra tests for gw.inf
#

from utils import *
import common


def test_haproxy(host):
    is_installed(host, 'haproxy')
    is_running_and_enabled(host, 'haproxy')
    is_listening(host, 'tcp://127.0.0.1:22')
    is_listening(host, 'tcp://127.0.0.1:80')
    is_listening(host, 'tcp://127.0.0.1:443')


def test_rules(host):
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 22 -m comment --comment "incoming ssh" -j ACCEPT')
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 80 -m comment --comment "incoming http" -j ACCEPT')
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 443 -m comment --comment "incoming https" -j ACCEPT')
    has_rule(host, '-A INPUT ! -i eth0 -p udp -m udp --dport 67 -m comment --comment "incoming DHCP requests" -j ACCEPT')
    has_rule(host, '-A INPUT ! -i eth0 -p udp -m udp --dport 5353 -j ACCEPT')
    has_rule(host, '-A INPUT ! -i eth0 -p tcp -m tcp --dport 5353 -j ACCEPT')

    has_rule(host, '-A OUTPUT -d 9.9.9.9/32 -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "outgoing dns" -j ACCEPT')
    has_rule(host, '-A OUTPUT -d 169.254.0.2/32 -o eth-inf -p tcp -m tcp --dport 80 -m comment --comment "outgoing pkg install" -j ACCEPT')
    has_rule(host, '-A OUTPUT -d 169.254.0.3/32 -o eth-inf -p tcp -m tcp --dport 1514 -m comment --comment "outgoing syslog" -j ACCEPT')
    has_rule(host, '-A OUTPUT -d 169.254.0.4/32 -o eth-inf -p tcp -m tcp --dport 8086 -m comment --comment "outgoing metrics" -j ACCEPT')
    has_rule(host, '-A OUTPUT -d 169.254.0.5/32 -o eth-inf -p tcp -m tcp --dport 4505:4506 -m comment --comment "outgoing salt" -j ACCEPT')
    has_rule(host, '-A OUTPUT -p udp -m udp --dport 68 -m comment --comment "outgoing DHCP replies" -j ACCEPT')

    has_rule(host, '-A FORWARD -i eth0 -j DROP')
    has_rule(host, '-A FORWARD -s 169.254.0.2/32 -i eth-inf -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "apt.inf fetching packages" -j ACCEPT')
    has_rule(host, '-A FORWARD -s 169.254.0.5/32 -i eth-inf -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "salt.inf fetching configs" -j ACCEPT')
    has_rule(host, '-A FORWARD -d 169.254.0.2/32 -o eth-inf -p tcp -m tcp --dport 80 -m comment --comment "hosts installing packages" -j ACCEPT')
    has_rule(host, '-A FORWARD -d 169.254.0.3/32 -o eth-inf -p tcp -m tcp --dport 1514 -m comment --comment "hosts syslog" -j ACCEPT')
    has_rule(host, '-A FORWARD -d 169.254.0.4/32 -o eth-inf -p tcp -m tcp --dport 8086 -m comment --comment "hosts metrics" -j ACCEPT')
    has_rule(host, '-A FORWARD -d 169.254.0.5/32 -o eth-inf -p tcp -m tcp --dport 4505:4506 -m comment --comment "hosts fetching configs" -j ACCEPT')
    has_rule(host, '-A FORWARD -s 10.0.1.2/32 -i eth-dev -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "lxd.dev egress 443" -j ACCEPT')
    has_rule(host, '-A FORWARD -s 10.0.2.2/32 -i eth-pub -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "wiki.pub egress 443" -j ACCEPT')
    has_rule(host, '-A FORWARD -s 10.0.3.2/32 -i eth-ctf -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "ctfd.ctf egress 443" -j ACCEPT')
    has_rule(host, '-A FORWARD -s 10.0.3.3/32 -i eth-ctf -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "web.ctf egress 443" -j ACCEPT')
    has_rule(host, '-A FORWARD -s 10.0.3.4/32 -i eth-ctf -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "pwn.ctf egress 443" -j ACCEPT')


def test_haproxy_reachable(host):
    can_connect(host, 'eth0', 22)
    can_connect(host, 'eth0', 80)
    can_connect(host, 'eth0', 443)


def test_ssh_not_reachable(host):
    cannot_connect(host, 'eth0', 2222)
