#
# testinfra tests for tig.inf
#

from utils import *
import common
import os


def test_influxdb_package(host):
    is_installed(host, 'influxdb')


def test_influxdb_running(host):
    is_running_and_enabled(host, 'influxdb')


def test_influxdb_listening(host):
    is_listening(host, 'tcp://0.0.0.0:8086')


def test_influxdb_reachable(host):
    can_connect(host, 'eth0', 8086)



def test_grafana_package(host):
    is_installed(host, 'grafana')


def test_grafana_running(host):
    is_running_and_enabled(host, 'grafana-server')


def test_grafana_listening(host):
    is_listening(host, 'tcp://127.0.0.1:3000')


def test_grafana_unreachable(host):
    cannot_connect(host, 'eth0', 3000)



def test_nginx_installed(host):
    is_installed(host, 'nginx-light')


def test_nginx_running(host):
    is_running_and_enabled(host, 'nginx')


def test_nginx_listening(host):
    is_listening(host, 'tcp://0.0.0.0:80')


def test_nginx_reachable(host):
    can_connect(host, 'eth0', 80)



def test_rules(host):
    # TODO std rules adapted
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 80 -m comment --comment "incoming http" -j ACCEPT')
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 8086 -m comment --comment "incoming metrics" -j ACCEPT')


def test_ssh_unreachable(host):
    common.ssh_not_reachable(host, 'eth0')
