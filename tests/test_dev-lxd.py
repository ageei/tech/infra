#
# testinfra tests for lxd.dev
#

from utils import *
import common


def test_lxd_packages(host):
    is_installed(host, 'lxd')
    is_installed(host, 'lxd-client')

def test_lxd_service(host):
    is_running_and_enabled(host, 'lxd')

def test_lxd_socket(host):
    is_listening(host, 'unix:///var/lib/lxd/unix.socket')


def test_gitlab_runner_package(host):
    is_installed(host, 'gitlab-runner')


def test_gitlab_runner_service(host):
    is_running_and_enabled(host, 'gitlab-runner')


def test_lxd_runner(host):
    g = host.file('/opt/lxd-runner-git')
    assert(g.exists)
    assert(g.is_directory)
    s = host.file('/opt/lxd-runner')
    assert(s.exists)
    assert(s.is_symlink)
    assert('/opt/lxd-runner-git' == s.linked_to)


def test_rules(host):
    common.std_rules(host)
    has_rule(host, '-A OUTPUT -d 10.0.0.0/8 -o eth0 -m comment --comment "drop to internal" -j DROP')
    has_rule(host, '-A OUTPUT -d 169.254.0.0/16 -o eth0 -m comment --comment "drop to internal .inf" -j DROP')
    has_rule(host, '-A OUTPUT -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "https to internet" -j ACCEPT')
    has_rule(host, '-A FORWARD -d 10.0.0.0/8 -i lxdbr0 -m comment --comment "ci drop to internal" -j DROP')
    has_rule(host, '-A FORWARD -d 169.254.0.0/16 -i lxdbr0 -m comment --comment "ci drop to internal .inf" -j DROP')
    has_rule(host, '-A FORWARD -i lxdbr0 -o eth0 -p tcp -m tcp --dport 443 -m comment --comment "lxd container https" -j ACCEPT')
    has_rule(host, '-A FORWARD -o lxdbr0 -m state --state RELATED,ESTABLISHED -j ACCEPT')


def test_ssh_not_reachable(host):
    common.ssh_not_reachable(host, 'eth0')
