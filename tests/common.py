#
# routines for tests common to two (2) or more hosts.
#

from utils import *


def ssh_not_reachable(host, iface):
    cannot_connect(host, iface, 22)


def std_rules(host):
    #
    # The standard rules are those related to:
    #
    #  - DNS on port udp:53 to 9.9.9.9
    #  - HTTP on port tcp:80 to apt.inf
    #  - RELP on port tcp:1514 to elk.inf
    #  - HTTP on port tcp:8086 to tig.inf
    #  - salt on port tcp:4505,4506 to salt.inf
    #
    # The only hosts not have _all_ these rules are { gw, apt, elk, tig , salt }.inf
    #
    ruleset = [
        '-A OUTPUT -d 9.9.9.9/32 -o eth0 -p udp -m udp --dport 53 -m comment --comment "outgoing dns" -j ACCEPT',
        '-A OUTPUT -d 169.254.0.2/32 -o eth0 -p tcp -m tcp --dport 80 -m comment --comment "outgoing pkg install" -j ACCEPT',
        '-A OUTPUT -d 169.254.0.3/32 -o eth0 -p tcp -m tcp --dport 1514 -m comment --comment "outgoing syslog" -j ACCEPT',
        '-A OUTPUT -d 169.254.0.4/32 -o eth0 -p tcp -m tcp --dport 8086 -m comment --comment "outgoing metrics" -j ACCEPT',
        '-A OUTPUT -d 169.254.0.5/32 -o eth0 -p tcp -m tcp --dport 4505:4506 -m comment --comment "outgoing salt" -j ACCEPT',
    ]
    [has_rule(host, r) for r in ruleset]
