#
# testinfra tests for salt.inf
#
# TODO
#  - has refresh cronjob
#

from utils import *


def test_salt_master_package(host):
    is_installed(host, 'salt-master')


def test_salt_master_service(host):
    is_running_and_enabled(host, 'salt-master')


def test_salt_master_reachable(host):
    can_connect(host, 'eth0', 4505)
    can_connect(host, 'eth0', 4506)


def test_rules(host):
    # TODO std rules minus salt
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 4505:4506 -m comment --comment "incoming salt" -j ACCEPT')
