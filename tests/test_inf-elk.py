#
# testinfra tests for elk.inf
#

from utils import *
import common
import os


def test_elasticsearch_installed(host):
    is_installed(host, 'elasticsearch')


def test_elasticsearch_running(host):
    is_running_and_enabled(host, 'elasticsearch')


# FIXME listens on ipv6
def test_elasticsearch_listening(host):
    #is_listening(host, 'tcp://127.0.0.1:9200')
    #is_listening(host, 'tcp://127.0.0.1:9300')
    pass


def test_elasticsearch_unreachable(host):
    cannot_connect(host, 'eth0', 9200)
    cannot_connect(host, 'eth0', 9300)



def test_logstash_installed(host):
    is_installed(host, 'logstash')


def test_logstash_running(host):
    is_running_and_enabled(host, 'logstash')


# FIXME listens on ipv6
def test_logstash_listening(host):
    #is_listening(host, 'tcp://127.0.0.1:1514')
    pass



def test_kibana_installed(host):
    is_installed(host, 'kibana')


def test_kibana_running(host):
    is_running_and_enabled(host, 'kibana')


def test_kibana_listening(host):
    is_listening(host, 'tcp://127.0.0.1:5601')



def test_nginx_installed(host):
    is_installed(host, 'nginx-light')


def test_nginx_running(host):
    is_running_and_enabled(host, 'nginx')


def test_nginx_listening(host):
    is_listening(host, 'tcp://0.0.0.0:80')


def test_nginx_reachable(host):
    can_connect(host, 'eth0', 80)



def test_rules(host):
    # TODO std rules minus nginx and logstash
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 80 -m comment --comment "incoming http" -j ACCEPT')


def test_ssh_not_reachable(host):
    common.ssh_not_reachable(host, 'eth0')
