#
# testinfra tests for pwn.ctf
#

from utils import *
import common


def test_lxd_packages(host):
    is_installed(host, 'lxd')
    is_installed(host, 'lxd-client')


def test_lxd_service(host):
    is_running_and_enabled(host, 'lxd')


def test_lxd_socket(host):
    is_listening(host, 'unix:///var/lib/lxd/unix.socket')


def test_openssh_packages(host):
    is_installed(host, 'openssh-server')


def test_openssh_service(host):
    is_running_and_enabled(host, 'ssh')


#def test_openssh(host):
    #is_listening(host, 'tcp://127.0.0.1:22')


def test_rules(host):
    common.std_rules(host)
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 22 -m comment --comment "incoming ssh" -j ACCEPT')
    has_rule(host, '-A OUTPUT -d 10.0.0.0/8 -o eth0 -m comment --comment "drop to internal" -j DROP')
    has_rule(host, '-A OUTPUT -d 169.254.0.0/16 -o eth0 -m comment --comment "drop to internal .inf" -j DROP')
    has_rule(host, '-A OUTPUT -o eth0 -p tcp -m tcp --dport 443 -m owner --uid-owner 0 -m comment --comment "ctf/pwn refresh" -j ACCEPT')


def test_ctf(host):
    u = host.user('ctf')
    assert(u)
    assert('/usr/src/ctfsh/shell.py' == u.shell)
    assert('/home/ctf' == u.home)
    assert('lxd' in u.groups)


def test_ctfsh(host):
    f = host.file('/usr/src/ctfsh/shell.py')
    assert(f.exists)
    assert(0o755 == f.mode)


def test_ssh_reachable(host):
    can_connect(host, 'eth0', 22)
