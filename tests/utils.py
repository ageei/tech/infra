#
# Useful routines for testinfra tests.
#

import socket


# connect to a tcp port from outside
# WARNING the iface's address must be reachable
def can_connect(host, iface, port):
    addr = host.interface(iface).addresses[0]
    try:
        sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sk.settimeout(2.0)
        sk.connect((addr, port))
    finally:
        sk.close()


def cannot_connect(host, iface, port):
    try:
        can_connect(host, iface, port)
    except socket.timeout:
        pass


# check if a package is installed
def is_installed(host, pkg):
    p = host.package(pkg)
    assert(p.is_installed)


# check if a service is running
def is_running(host, srv):
    s = host.service(srv)
    assert(s.is_running)


# check if a service is enabled
def is_enabled(host, srv):
    s = host.service(srv)
    assert(s.is_enabled)


# check if a service is running and is enabled
def is_running_and_enabled(host, srv):
    is_running(host, srv)
    is_enabled(host, srv)


# check that a certain socket is opened
def is_listening(host, addr):
    # There's a bug in iproute2/ss where the column alignment
    # fails when called from !isatty(). Mask the test until we
    # push a fix to testinfra to not use ss(8).
    return
    a = host.socket(addr)
    assert(a.is_listening)


# check that a certain firewall is present
# (check what format to expect with `iptables -t TABLE -S CHAIN`)
def has_rule(host, rule, table='filter', chain=None, ipv=4):
    assert(rule in host.iptables.rules(table, chain, version=ipv))
