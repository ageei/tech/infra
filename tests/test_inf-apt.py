#
# testinfra tests for apt.inf
#
# TODO
# - verify package acquisition
#

from utils import *
import common


def test_apt_cacher_ng_package(host):
    is_installed(host, 'apt-cacher-ng')


def test_apt_cacher_ng_service(host):
    is_running_and_enabled(host, 'apt-cacher-ng')


def test_apt_cacher_ng_listening(host):
    is_listening(host, 'tcp://127.0.0.1:3142')


def test_nginx_package(host):
    is_installed(host, 'nginx-light')


def test_nginx_service(host):
    is_running_and_enabled(host, 'nginx')


def test_nginx_listening(host):
    is_listening(host, 'tcp://0.0.0.0:80')


def test_rules(host):
    has_rule(host, '-A INPUT -i eth0 -p tcp -m tcp --dport 80 -m comment --comment "incoming http" -j ACCEPT')


def test_nginx_reachable(host):
    can_connect(host, 'eth0', 80)


def test_apt_cacher_ng_not_reachable(host):
    cannot_connect(host, 'eth0', 3142)


def test_ssh_not_reachable(host):
    common.ssh_not_reachable(host, 'eth0')
