#!/bin/sh
##
set -eu

V=1.4.2


apt-get -y install qemu-system-x86 unzip wget

cd /tmp
wget "https://releases.hashicorp.com/packer/${V}/packer_${V}_linux_amd64.zip"
unzip ./packer*.zip
