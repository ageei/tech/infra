#!/bin/sh
#
# Encrypt a secret using a given keyring.
# If no keyring is specified, use the current directory's.
#
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)


if [ X = X"${GNUPGHOME:-}" ]; then
	GNUPGHOME="${PROGBASE}/gnupg"
	export GNUPGHOME
fi

exec gpg \
	--armor \
	--batch \
	--trust-model always \
	--encrypt -r ageei
