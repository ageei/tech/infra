#!/bin/sh
#
# Generate a temporary keyring in the current directory.
#
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)


if [ X = X"${GNUPGHOME:-}" ]; then
	GNUPGHOME="${PROGBASE}/gnupg"
	export GNUPGHOME
fi


params() {
	cat<<__EOF__
     %echo Generating a basic OpenPGP key
     Key-Type: RSA
     Key-Length: 2048
     Subkey-Type: RSA
     Subkey-Length: 2048
     Name-Real: Temporary AGEEI
     Name-Comment: passphrase:password
     Name-Email: ageei@example.com
     Expire-Date: 0
     Passphrase: password
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
__EOF__
}


[ -d "${GNUPGHOME}" ] || mkdir "${GNUPGHOME}"

chmod 0700 "${GNUPGHOME}"
params | gpg --batch --generate-key
gpg --armor -o key.priv --export-secret-key ageei
gpg --armor -o key.pub --export ageei

printf '[I] Use the keyring with gpg --homedir="%s" ...\n' "${GNUPGHOME}"
printf '\n\nDO NOT USE IN PRODUCTION!\n\n\n'
