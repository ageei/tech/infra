#!/bin/sh
#
# Generate a production keyring in the current directory.
#
set -eu

PROGNAME=$(basename -- "${0}")
PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)

if [ X = X"${GNUPGHOME:-}" ]; then
	printf '[E] GNUPGHOME=... sh %s\n' "${PROGNAME}" >&2
	exit 2
fi


params() {
	cat<<__EOF__
     %echo Generating an OpenPGP key
     Key-Type: RSA
     Key-Length: 4096
     Subkey-Type: RSA
     Subkey-Length: 4096
     Name-Real: AGEEI UQAM
     Name-Comment: Clef des secrets
     Name-Email: president@ageei.org
     Expire-Date: 0
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
__EOF__
}


chmod 0700 "${GNUPGHOME}"
params | gpg --batch --generate-key

# instructions
cat<<__EOF__
gpg --armor -o key.priv --export-secret-key ageei
gpg --armor -o key.pub --export ageei
__EOF__
