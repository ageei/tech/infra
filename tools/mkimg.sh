#!/bin/sh
##
set -eu

PROJROOT=$(d=$(dirname -- "${0}"); cd "${d}/.." && pwd)


CI_ACCESS="${CI_ACCESS:-}"
CI_IMAGE="${CI_IMAGE:-ubuntu:18.04}"
CI_PROFILE="${CI_PROFILE:-ageei-ci}"
CI_ENVIRON="${CI_ENVIRON:-dev}"
EXTRA_INIT_ARGS="${EXTRA_INIT_ARGS:-}"

trap "lxc delete --force '${1}'" EXIT INT QUIT TERM


# failure marker
fmark=


cd "${PROJROOT}"
[ -d ./dist ] || mkdir ./dist/

if ! lxc profile ls | grep -q "[[:space:]]${CI_PROFILE}[[:space:]]"; then
	printf '[I] Creating %s profile\n' "${CI_PROFILE}"
	lxc profile create "${CI_PROFILE}"
fi
printf '[I] Updating profile\n'
sed -e "s|@env@|${CI_ENVIRON}|" ./.ci/cloud-init.yml | \
	lxc profile edit "${CI_PROFILE}"

printf '[I] Initializing the container\n'
lxc init \
	-p default \
	${EXTRA_INIT_ARGS} \
	-p "${CI_PROFILE}" \
	"${CI_IMAGE}" \
	"${1}"


printf '[I] Pushing saltstack files\n'
lxc file push ./saltstack/pillar "${1}/srv/" -r
lxc file push ./saltstack/salt   "${1}/srv/" -r


printf '[I] Starting container\n'
lxc start "${1}"

printf '[I] Waiting for cloud-init to complete\n'
sleep 10
while :; do
	if lxc exec "${1}" -- cloud-init status | grep -q 'running$'; then
		printf '='
		sleep 3
	else
		break
	fi
done
printf '=>\n'


status=$(lxc exec "${1}" -- /bin/sh -c 'cloud-init status || :')
if echo "${status}" | grep -q 'error$'; then
	fmark=1
	printf '[E] Deployment failed\n'
else
	printf '[I] Deployment succeeded\n'
fi
lxc exec "${1}" -- /bin/cat /var/log/cloud-init-output.log


if [ X != X"${CI_TESTS:-}" ]; then
	printf '[I] Launching tests\n'
	# wait for everything to be up and running
	sleep 1
	cd ./tests/
	py.test -v --hosts="lxc://${1}" "test_${1}.py" "test_all.py" || fmark=1
fi


if [ X != X"${CI_ACCESS}" ]; then
	lxc exec "${1}" -- /bin/bash || :
fi


printf '[I] Stopping container\n'
if [ X = X"${fmark}" ] && [ X != X"${CI_BUILD:-}" ]; then
	# Remove private keys from images.
	lxc exec "${1}" -- /bin/sh -c 'rm -rf /etc/ssh/ssh_host_* /etc/letsencrypt; apt-get clean; rm -rf /var/lib/apt/lists/*'
fi
lxc stop "${1}"


if [ X = X"${fmark}" ] && [ X != X"${CI_BUILD:-}" ]; then
	printf '[I] Creating image\n'
	lxc publish "${1}" --alias "${1}"
	lxc image export "${1}" "${PROJROOT}/dist/${1}"
	lxc image rm "${1}"
	exit 0
fi

[ X = X"${fmark}" ] || exit 1
exit 0
