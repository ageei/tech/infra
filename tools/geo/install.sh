#!/bin/sh
# ==================================================================== #
#
# This script installs the base image on a disk.
# It is meant to run with packer (https://packer.io/):
#
#   packer build packer.json
#
# ==================================================================== #
set -eux

BASEURL=https://cloud-images.ubuntu.com/releases/18.04/release/
TARBALL=ubuntu-18.04-server-cloudimg-amd64-root.tar.xz
SHAFILE=SHA256SUMS

DISK="${1}"



DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

# GPG signature keys are expired on the ISO. We don't need
# packages under the testing archive so it is safe to ignore.
apt-get update || :
apt-get -y install gnupg2 lvm2

# Download Ubuntu's signature key.
gpg --keyserver keyserver.ubuntu.com --recv-keys 0xd2eb44626fddc30b513d5bb71a5d6c4c7db87c81 || :


cd /tmp
wget -q "${BASEURL}/${TARBALL}"
wget -q "${BASEURL}/${SHAFILE}"
wget -q "${BASEURL}/${SHAFILE}.gpg"
gpg --verify "${SHAFILE}.gpg" "${SHAFILE}"
sha256sum --ignore-missing -c "${SHAFILE}"


tmpdir=$(mktemp -d)
cleanup() {
	mount | cut -d' ' -f3 | grep "^${tmpdir}" | sort -r | xargs -n1 umount
}
trap cleanup EXIT INT QUIT TERM


parts() {
	# BIOS MBR, 1:512M, 2:+
	printf 'n\np\n1\n\n+512M\n'
	printf 'n\np\n2\n\n\n'
	printf 'p\nw\nq\n'
}



# partitioning
dd if=/dev/zero of="${DISK}" bs=4096 count=4096
parts | fdisk "${DISK}"
pvcreate "${DISK}2"
vgcreate vg.geo "${DISK}2"


# filesystems
mkfs.ext4 -F "${DISK}1"
for p in root:1G: usr:6G:usr var:4G:var home:512m:home; do
	n=$(echo "${p}" | cut -d: -f1)
	t=$(echo "${p}" | cut -d: -f3)

	lvcreate -n "${n}" -L $(echo "${p}" | cut -d: -f2) vg.geo
	mkfs.ext4 -F "/dev/vg.geo/${n}"
	[ -d "${tmpdir}/${t}" ] || mkdir "${tmpdir}/${t}"
	mount "/dev/vg.geo/${n}" "${tmpdir}/${t}"
done

[ -d "${tmpdir}/boot" ] || mkdir "${tmpdir}/boot"
mount /dev/vda1 "${tmpdir}/boot"


# install the OS
xz -cd "/tmp/${TARBALL}" | (cd "${tmpdir}" && tar -f- -px)


# prepare to chroot
mount -t proc none "${tmpdir}/proc"
mount --rbind /dev/ "${tmpdir}/dev"
mount --rbind /sys/ "${tmpdir}/sys"
mount --make-rslave "${tmpdir}/dev"
mount --make-rslave "${tmpdir}/sys"

cat >"${tmpdir}/root/install.sh"<<__EOF__
#!/bin/sh
set -eux

/bin/rm -f /etc/resolv.conf
echo nameserver 8.8.8.8 >/etc/resolv.conf

apt-get update
DEBIAN_FRONTEND=noninteractive \
apt-get -y install \
	grub-pc \
	initramfs-tools \
	linux-image-generic \
	lvm2


addfs() {
	printf UUID=
	printf '%s' \$(blkid -sUUID -ovalue "\${1}")
	printf ' %s ext4 defaults 0 %s\n' "\${2}" "\${3}"
}
addlv() {
	addfs "/dev/mapper/vg.geo-\${1}" "\${2}" "\${3}"
}

addfs /dev/vda1 /boot 2 >/etc/fstab
addlv root      /     1 >>/etc/fstab
# broken Ubuntu assumes /usr is on the same partition as /
# and won't boot correctly when identified by UUID.
#addlv usr       /usr  2 >>/etc/fstab
printf '/dev/mapper/vg.geo-usr /usr ext4 defaults 2\n' >>/etc/fstab
addlv var       /var  2 >>/etc/fstab
addlv home      /home 2 >>/etc/fstab
cat /etc/fstab

# make sure we can always access the console
echo GRUB_CMDLINE_LINUX_DEFAULT='console=ttyS0 console=tty0' >>/etc/default/grub
echo GRUB_TERMINAL=console >>/etc/default/grub
echo unset GRUB_TIMEOUT_STYLE >>/etc/default/grub
echo GRUB_TIMEOUT=5 >>/etc/default/grub
echo unset GRUB_HIDDEN_TIMEOUT >>/etc/default/grub
echo unset GRUB_HIDDEN_TIMEOUT_QUIET >>/etc/default/grub
# remove cloud-init stuff that won't give us a grub menu
rm -rf /etc/default/grub.d

grub-install /dev/vda
update-initramfs -u
update-grub

echo root:ageei | chpasswd
__EOF__

chroot "${tmpdir}" /bin/sh /root/install.sh
sync

# ==================================================================== #
