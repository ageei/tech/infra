#!/bin/sh
#
set -eu

DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

apt-get update
apt-get -y upgrade
apt-get -y install gnupg2

# write to /etc/apt/sources.list so
# it gets overwritten when we switch to salt.
echo "deb https://repo.saltstack.com/apt/ubuntu/18.04/amd64/2019.2 bionic main" >>/etc/apt/sources.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0x754A1A7AE731F165D5E6D4BD0E08A149DE57BFBE

apt-get update
apt-get -y install git python-pygit2 salt-minion

# ensure salt knows it's the geo system
echo inf-geo >/etc/salt/minion_id

# install zfs
apt-get -y install \
	zfs-zed \
	zfsutils-linux

modprobe zfs
