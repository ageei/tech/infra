#!/bin/sh
#
# Run geo in qemu.
#
# WARNING Assumes a Linux-based system with qemu 3 installed.
#
# This configuration will:
#
#  - create a secondary drive for the zfs pool `vpool`;
#  - share this script's grand-parent directory;
#  - redirect ports 8022, 8080 and 8443 to ports 22, 80 and 443
#    of the VM.
#
#
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)
PROJROOT=$(cd "${PROGBASE}/../.." && pwd)


DISK="${PROGBASE}/output-geo/packer-geo"
VIRT="${PROGBASE}/zfs.hdd"


if [ ! -f "${VIRT}" ]; then
	printf '[I] Creating ZFS storage drive\n'
	qemu-img create -f qcow2 "${VIRT}" 16G
fi


exec qemu-system-x86_64 \
	-name ageei.org \
	-enable-kvm \
	-m 16G \
	-cpu host \
	-smp cpus=4 \
	-drive "file=${DISK},if=virtio" \
	-drive "file=${VIRT},if=virtio" \
	-fsdev "local,security_model=none,id=fsdev0,path=${PROJROOT}" \
	-device virtio-9p-pci,id=fs0,fsdev=fsdev0,mount_tag=shared \
	-netdev user,id=i0,net=10.255.255.0/24,hostfwd=tcp:0.0.0.0:8080-10.255.255.16:80,hostfwd=tcp:0.0.0.0:8022-10.255.255.16:22,hostfwd=tcp:0.0.0.0:8443-10.255.255.16:443 \
	-device e1000,netdev=i0 \
	-vga virtio \
	"${@}"
