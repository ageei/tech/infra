#!/bin/sh
# ==================================================================== #
#
# Sends a message on Slack using curl.
#
# ==================================================================== #
set -eu

PROGNAME=$(basename -- "${0}")


usage() {
	printf 'usage: XOXP=<xoxp> %s [-h] <channel> <text>\n' \
		"${PROGNAME}"
}

while getopts h- argv; do
	case "${argv}" in
	h)	usage
		exit 0
		;;
	-)	break
		;;
	*)	usage >&2
		exit 2
		;;
	esac
	shift $(( ${OPTIND} + 1 ))
done

[ X-- != X"${1:-}" ] || shift


if [ X2 != X"${#}" ]; then
	usage >&2
	exit 2
fi

# -------------------------------------------------------------------- #

exec curl \
	-X POST \
	-H 'Content-Type: application/json; charset=utf-8' \
	-H "Authentication: Bearer ${XOXP}" \
	https://slack.com/api/chat.postMessage \
	-d '{"channel":"'"${1}"'","username":"test","text":"'"${2}"'"}'

# ==================================================================== #
