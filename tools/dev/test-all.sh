#!/bin/sh
set -eu

PROJROOT=$(d=$(dirname -- "${0}"); cd "${d}/../.." && pwd)


cd "${PROJROOT}"
. ./venv/bin/activate


CI_TESTS=1
export CI_TESTS


for s in ./tests/test_*.py; do
	echo "${s}" | grep -qv -- '_all\.py$' || continue
	n=$(echo "${s}" | cut -d_ -f2 | cut -d. -f1)

	case "${n}" in
	inf-gw)
		EXTRA_INIT_ARGS="${EXTRA_INIT_ARGS:-} -pinf-gw" sh ./tools/mkimg.sh "${n}" || :
		;;
	*)
		sh ./tools/mkimg.sh "${n}" || :
		;;
	esac
done
